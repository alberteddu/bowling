# Bowling

React app that uses a fork of Create React App with CSS modules.
You can check the finished project at http://bowlingwoot.surge.sh

To test locally, first install the dependencies:

    npm i # or
    yarn
    
Then start the app:

    npm start # or
    yarn start
    
## Directory structure

The application components are stored in "components". As this application uses Redux,
containers (or "smart" components) are in "containers". "functions" contains functionality
shared by other parts of the code.

Models contain prop types and constants used in different components.

Everything related to the store (and redux) is inside "store".

There is a generic global CSS in "styles", but all other styles use CSS modules with class
rename on build. When the application is built, the styles are extracted to an external CSS file.

## Comments about the implementation

While the directory structure does satisfy me, in general there are a few choices I had to
make in order to deliver the challenge in time. I didn't really take a mobile first approach
and assumed the application would run on a desktop only (although it does work on mobile).

If I had to plan this with more development time, I would probably add first a stable formalization
of all the entities and the various actions that they can carry out. 

For example, it would be nice to have a nicer representation of a Player, a Game, and the Score 
of a Player in a game. The Game class could have methods that make it easier to calculate
which player is currently throwing the ball. I think this approach would result in cleaner
code, easier refactorings, and could make it easier to avoid edge case bugs or 
inconsistencies in the long run.

While I have tried my application extensively, I am pretty sure it still has a few bugs. But
I guess it is important that it works "a bit" and that you can see the approach I went for.

The application has no tests. I do like to write them, but I thought it would have been really easy
to get lost in the unit testing and I wanted to deliver something that is kind of nice to see
and it also actually works. 

In a normal scenario, I would not write a single component or style until the framework of entities
and their related tests is there. This is not also pretty fun, when done right, but it also helps me 
have a clearer idea of how I want to structure things. Writing tests first allows me to use
the core API in the best way without it actually having to exist. After that, implementing
it is fast, and there's less danger of changing course in the middle of things. Then, the last step
of adding smart and dumb components is quick and satisfying, because one can see everything come
to life, while being sure that it probably works.

If you have any questions, feel free to ask me.
