import { connect } from 'react-redux';
import { Games as DumbGames } from '../../components/Games';
import { orderedGamesSelector } from '../../store/selectors';

const mapStateToProps = state => ({
    games: orderedGamesSelector(state),
});

export const Games = connect(mapStateToProps)(DumbGames);
