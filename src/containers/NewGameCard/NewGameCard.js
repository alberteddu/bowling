import { connect } from 'react-redux';
import { compose, withProps } from 'recompose';
import { CardAction } from '../../components/CardAction';
import { startCreatingGame } from '../../store/actions';

const mapDispatchToProps = dispatch => ({
    action: () => dispatch(startCreatingGame()),
});

export const NewGameCard = compose(
    connect(null, mapDispatchToProps),
    withProps({
        label: 'Create new game',
    }),
)(CardAction);
