import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Route } from 'react-router-dom';
import { ConnectedRouter } from 'react-router-redux';
import ReactModal from 'react-modal';
import { history } from '../../store';
import { Games } from '../../routes/Games';
import { Game } from '../../routes/Game';
import { modalOpenSelector } from '../../store/selectors';
import { stopEditingGame } from '../../store/actions';
import { GameEditView } from '../../containers/GameEditView';

const BaseApp = ({
    open,
    onClose,
}) => (
    <ConnectedRouter history={history}>
        <div id="application">
            <ReactModal
                isOpen={open}
                ariaHideApp={false}
                onRequestClose={onClose}
            >
                <GameEditView />
            </ReactModal>
            <Route exact path="/" component={Games} />
            <Route exact path="/g/:id" component={Game} />
        </div>
    </ConnectedRouter>
);

BaseApp.propTypes = {
    open: PropTypes.bool.isRequired,
    onClose: PropTypes.func.isRequired,
};

const mapStateToProps = state => ({
    open: modalOpenSelector(state),
});

const mapDispatchToProps = dispatch => ({
    onClose: () => dispatch(stopEditingGame()),
});

export const App = connect(mapStateToProps, mapDispatchToProps)(BaseApp);
