import { connect } from 'react-redux';
import { compose, withProps } from 'recompose';
import { GameEditView as DumbGameEditView } from '../../components/GameEditView';
import { creatingSelector, editingSelector, gamesSelector } from '../../store/selectors';
import { createGame, stopEditingGame } from '../../store/actions';

const mapStateToProps = state => ({
    creating: creatingSelector(state),
    editing: editingSelector(state),
    games: gamesSelector(state),
});

const mapDispatchToProps = dispatch => ({
    createGame: (...attrs) => {
        dispatch(createGame(...attrs));
        dispatch(stopEditingGame());
    },
});

export const GameEditView = compose(
    connect(mapStateToProps, mapDispatchToProps),
    withProps(props => (props.creating ? {
        onSave: (...attrs) => {
            const ids = props.games.map(g => parseInt(g.id, 10));
            const nextId = ids.length > 0 ? Math.max(...ids) + 1 : 1;

            props.createGame(nextId, ...attrs);
        },
        title: 'Untitled game',
        players: [],
    } : {
        // game:
        // onSave: props.saveGame, todo: uncomment this when implementing edit
    })),
)(DumbGameEditView);
