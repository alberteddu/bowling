export const isGameValid = (title, players) => {
    const validPlayer = id => players[id].id && players[id].name.trim() !== '';

    if (title.trim() === '') {
        return false;
    }

    if (Object.keys(players).length === 0) {
        return false;
    }

    if (Object.keys(players).filter(p => !validPlayer(p)).length > 0) {
        return false;
    }

    return true;
};
