const sentences = {
    1: [
        'Wow, you won!',
        'Were you cheating?',
        'Good job, you won.',
    ],
    2: [
        'Very good, congrats!',
        'Nice silver.',
        'Well played!',
    ],
    3: [
        'Bronze for you, not bad.',
        'Good third place!',
        'Good result',
    ],
    other: [
        'Better next time',
        'Not bad, but not good',
    ],
};

export const getPlaceSentence = (place) => {
    const index = place < 4 ? place : 'other';
    const items = sentences[index];

    return items[Math.floor(Math.random() * items.length)];
};
