export const progressGame = (game, playerId, pins) => {
    return {
        ...game,
        players: game.players.map((player) => {
            if (player.id === playerId) {
                const { score } = player;
                const newScore = [...score];

                // This is the first attempt of the first frame
                // Add a new array with the first attempt result.
                if (score.length === 0) {
                    newScore.push([pins]);

                    // If this was a strike, add placeholder for second attempt
                    if (pins === 10) {
                        newScore[0].push(null);
                    }
                }

                // Not the first or the last frame
                if (score.length > 0 && score.length < 10) {
                    const lastItem = score[score.length - 1];

                    // This is the first attempt of the new frame
                    if (lastItem.length === 2) {
                        newScore.push([pins]);

                        // If this was a strike, add placeholder for second attempt
                        // This should not happen if this is the last frame.
                        if (pins === 10 && score.length < 9) {
                            newScore[newScore.length - 1].push(null);
                        }
                    } else {
                        // Or, the second attempt of the last frame
                        newScore[newScore.length - 1].push(pins);
                    }
                }

                // Last frame
                if (score.length === 10) {
                    const lastFrame = score[9];

                    // This is the third attempt
                    if (lastFrame.length === 2) {
                        lastFrame.push(pins);
                    }

                    // This is the second attempt
                    if (lastFrame.length === 1) {
                        lastFrame.push(pins);

                        // If the first attempt wasn't a strike,
                        // and the current attempt also wasn't a strike,
                        // the player has no more attempts, so add a placeholder.
                        if (lastFrame[0] < 10 && pins < 10) {
                            lastFrame.push(null);
                        }
                    }
                }

                return {
                    ...player,
                    score: newScore,
                };
            }

            return player;
        }),
    };
};
