export const FIRST_ATTEMPT = 'FIRST_ATTEMPT';
export const SECOND_ATTEMPT = 'SECOND_ATTEMPT';
export const THIRD_ATTEMPT = 'THIRD_ATTEMPT';
export const NO_MORE_ATTEMPTS = 'NO_MORE_ATTEMPTS';

export const getBallAttempt = (score, last = false) => {
    if (score[0] === undefined) {
        return FIRST_ATTEMPT;
    }

    if (score[1] === undefined) {
        return SECOND_ATTEMPT;
    }

    if (!last) {
        return NO_MORE_ATTEMPTS;
    }

    return score[2] === undefined ? THIRD_ATTEMPT : NO_MORE_ATTEMPTS;
};
