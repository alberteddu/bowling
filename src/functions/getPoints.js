export const getPoints = (index, score) => {
    let points = 0;

    for (let i = 0; i <= index; i += 1) {
        if (!score[i]) {
            break;
        }

        const currentScore = score[i];
        const nextScore = score[i + 1];
        const nextNextScore = score[i + 2];
        const getAttempt = (attempt, nth) => attempt[nth];
        const isAttemptValid = (attempt, nth) => attempt && getAttempt(attempt, nth) !== undefined;
        const currentAttempt = n => getAttempt(currentScore, n);
        const currentAttemptValid = n => isAttemptValid(currentScore, n);
        const nextAttempt = n => getAttempt(nextScore, n);
        const nextAttemptValid = n => isAttemptValid(nextScore, n);
        const nextNextAttempt = n => getAttempt(nextNextScore, n);
        const nextNextAttemptValid = n => isAttemptValid(nextNextScore, n);

        points += currentAttempt(0);

        if (currentAttemptValid(1)) {
            points += currentAttempt(1);
        }

        // If this is the last frame, take into account third attempt.
        if (index === 9) {
            if (currentAttempt(0) === 10) {
                if (currentAttemptValid(2)) {
                    points += currentAttempt(2);
                }
            }

            if (currentAttempt(0) !== 10 && currentAttempt(0) + currentAttempt(1)) {
                if (currentAttemptValid(2)) {
                    points += currentAttempt(2);
                }
            }
        }

        // Strike takes into account next 2 balls
        if (currentAttempt(0) === 10 && nextAttemptValid(0)) {
            points += nextAttempt(0);

            // Definitely consider the next attempt,
            // as long as it's there.
            if (nextAttemptValid(1)) {
                points += nextAttempt(1);
            }

            // Consider the first attempt of the next+1 score
            // only if the next attempt was a strike also.
            // Otherwise "two attempts" means the first and
            // the second one of the next frame.
            if (nextNextAttemptValid(0) && nextAttempt(0) === 10) {
                points += nextNextAttempt(0);
            }
        }

        // Spare takes into account next ball
        if (
            currentAttempt(0) < 10 // Not a strike
            && currentAttempt(0) + currentAttempt(1) === 10 // But a spare
            && nextAttemptValid(0) // And there is a next attempt
        ) {
            points += nextAttempt(0);
        }
    }

    return points;
};
