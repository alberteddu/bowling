import { isGameOver } from './isGameOver';

export const findTurnPlayer = (players) => {
    if (isGameOver(players)) {
        return 0;
    }

    let min = 10;
    let max = 0;

    players.forEach((p) => {
        let size = p.score.length;

        if (size === 10 && p.score[9].length < 3) {
            size = 9;
        }

        if (size > 0 && size < 10 && p.score[p.score.length - 1].length < 2) {
            size -= 1;
        }

        if (size < min) {
            min = size;
        }

        if (size > max) {
            max = size;
        }
    });

    // First player starts again
    if (min === max) {
        return 0;
    }

    return players.indexOf(players.find((p) => {
        let size = p.score.length;

        if (size === 10 && p.score[9].length < 3) {
            size = 9;
        }

        if (size > 0 && size < 10 && p.score[p.score.length - 1].length < 2) {
            size -= 1;
        }

        return size === min;
    }));
};

