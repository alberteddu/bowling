import { FIRST_ATTEMPT, SECOND_ATTEMPT, THIRD_ATTEMPT } from './getBallAttempt';

export const getAttemptString = (attempt) => {
    switch (attempt) {
        case FIRST_ATTEMPT:
            return 'first attempt';
        case SECOND_ATTEMPT:
            return 'second attempt';
        case THIRD_ATTEMPT:
            return 'third attempt';
        default:
            return false;
    }
};
