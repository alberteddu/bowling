import { getPoints } from './getPoints';

export const getFrameProps = (index, score) => {
    const props = {};

    if (index === 9) {
        props.last = true;
    }

    if ((!score[index] && index === 0)
        || (
            !score[index] &&
            score[index - 1] &&
            score[index - 1][0] !== undefined &&
            score[index - 1][1] !== undefined
        )) {
        props.current = true;
        return props;
    }

    if (!score[index]) {
        return props;
    }

    const frame = score[index];

    [props.pins, props.secondChance, props.thirdChance] = frame;

    props.strike = frame[0] === 10;
    props.spare = !props.strike && frame[0] + frame[1] === 10;
    props.secondChanceStrike = props.strike && frame[1] === 10;
    props.secondChanceSpare = !props.secondChanceStrike && frame[1] + frame[2] === 10;
    props.thirdChanceStrike = frame[2] === 10;

    if (!score[index + 1] && (score[index][0] === undefined || score[index][1] === undefined)) {
        props.current = true;
    }

    if (index === 9 && props.strike && frame.length < 3) {
        props.current = true;
    }

    props.points = getPoints(index, score);

    return props;
};
