export const isGameOver = (players) => {
    for (let i = 0; i < players.length; i += 1) {
        const player = players[i];
        const score = player.score;

        if (score.length < 10) {
            return false;
        }

        if (score[9].length < 3) {
            return false;
        }
    }

    return true;
};
