import { createSelector } from 'reselect';
import { gameSelector } from './game';

export const playersSelector = id => createSelector(
    gameSelector(id),
    game => game.players,
);
