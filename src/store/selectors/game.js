import { createSelector } from 'reselect';
import { IN_PROGRESS, OVER } from '../../models/Game';
import { isGameOver } from '../../functions/isGameOver';

export const plainGamesSelector = state => state.game.games;
export const gamesSelector = createSelector(
    plainGamesSelector,
    games => games.map(g => ({
        ...g,
        state: isGameOver(g.players) ? OVER : IN_PROGRESS,
    })),
);
export const creatingSelector = state => state.game.creating;
export const editingSelector = state => state.game.editing;

export const orderedGamesSelector = createSelector(
    gamesSelector,
    games => games.sort((a, b) => {
        if (a.state === OVER && b.state !== OVER) {
            return 1;
        }

        if (b.state === OVER && a.state !== OVER) {
            return -1;
        }

        return b.id - a.id;
    }),
);

export const modalOpenSelector = createSelector(
    creatingSelector,
    editingSelector,
    (creating, editing) => creating || editing,
);

export const gameSelector = id => createSelector(
    gamesSelector,
    games => games.find(g => g.id === id),
);

export const currentGameSelector = createSelector(
    gamesSelector,
    (state, props) => props.id,
    (games, id) => games.find(g => g.id === id),
);

