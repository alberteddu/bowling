import { IN_PROGRESS } from '../../models/Game';

export const CREATE_GAME = 'CREATE_GAME';
export const createGame = (id, name, players = []) => ({
    type: CREATE_GAME,
    game: {
        id,
        name,
        players,
        state: IN_PROGRESS,
    },
});

export const DELETE_GAME = 'DELETE_GAME';
export const deleteGame = id => ({
    type: DELETE_GAME,
    id,
});

export const START_CREATING_GAME = 'START_CREATING_GAME';
export const startCreatingGame = () => ({
    type: START_CREATING_GAME,
});

export const START_EDITING_GAME = 'START_EDITING_GAME';
export const startEditingGame = id => ({
    type: START_EDITING_GAME,
    id,
});

export const STOP_EDITING_GAME = 'STOP_EDITING_GAME';
export const stopEditingGame = () => ({
    type: STOP_EDITING_GAME,
});

export const PROGRESS = 'PROGRESS';
export const progress = (gameId, playerId, pins) => ({
    type: PROGRESS,
    gameId,
    playerId,
    pins,
});
