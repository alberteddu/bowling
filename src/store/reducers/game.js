import {
    CREATE_GAME,
    DELETE_GAME,
    START_CREATING_GAME,
    START_EDITING_GAME,
    STOP_EDITING_GAME,
    PROGRESS,
} from '../actions';
import { progressGame } from '../../functions/progressGame';

const defaultState = {
    editing: false,
    creating: false,
    editId: null,
    games: [
        {
            id: 1,
            name: 'Some game',
            players: [
                {
                    id: 1,
                    name: 'Noonjackal Muse',
                    score: [],
                },
                {
                    id: 2,
                    name: 'Cottonkeeper Lion',
                    score: [],
                },
                {
                    id: 3,
                    name: 'Plaineater Thorn',
                    score: [],
                },
            ],
        },
        {
            id: 2,
            name: 'Another game',
            players: [
                {
                    id: 1,
                    name: 'Prairiewizard Brow',
                    score: [
                        [10, 0],
                        [3, 0],
                        [3, 5],
                        [5, 4],
                        [3, 5],
                        [10, 0],
                        [5, 5],
                        [9, 1],
                        [10, 0],
                        [10, 5, 5],
                    ],
                },
                {
                    id: 2,
                    name: 'Forkhare Runner',
                    score: [
                        [10, 0],
                        [10, 0],
                        [3, 5],
                        [3, 4],
                        [3, 6],
                        [4, 0],
                        [0, 3],
                        [9, 1],
                        [5, 0],
                        [3, 7, 5],
                    ],
                },
                {
                    id: 3,
                    name: 'Buttercuppaw Crusher',
                    score: [
                        [10, 0],
                        [10, 0],
                        [3, 2],
                        [3, 4],
                        [3, 6],
                        [4, 0],
                        [0, 3],
                        [9, 1],
                        [5, 0],
                        [10, 10, 3],
                    ],
                },
            ],
        },
    ],
};

export default (state = defaultState, action) => {
    switch (action.type) {
        case CREATE_GAME:
            return {
                ...state,
                games: [
                    ...state.games,
                    action.game,
                ],
            };
        case DELETE_GAME:
            return {
                ...state,
                games: [
                    ...state.games.filter(g => g.id !== action.id),
                ],
            };
        case START_CREATING_GAME:
            return {
                ...state,
                creating: true,
                editing: false,
                editId: null,
            };
        case START_EDITING_GAME:
            return {
                ...state,
                creating: false,
                editing: true,
                editId: action.id,
            };
        case STOP_EDITING_GAME:
            return {
                ...state,
                editing: false,
                creating: false,
                editId: null,
            };
        case PROGRESS:
            return {
                ...state,
                games: [
                    ...state.games.map((g) => {
                        if (g.id === action.gameId) {
                            return progressGame(g, action.playerId, action.pins);
                        }

                        return g;
                    }),
                ],
            };
        default:
            return state;
    }
};
