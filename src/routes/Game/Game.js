import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { compose, withProps } from 'recompose';
import { currentGameSelector } from '../../store/selectors';
import { Game as GameShape } from '../../models/Game';
import { Scores } from '../../components/Scores';
import { progress } from '../../store/actions';

const DumbGame = ({
    game,
    onProgress,
}) => (
    <React.Fragment>
        <h2>{game.name}
            <small><a href="/#/">Back to all games</a></small>
        </h2>
        <Scores state={game.state} players={game.players} onProgress={onProgress} />
    </React.Fragment>
);

DumbGame.propTypes = {
    game: GameShape.isRequired,
    onProgress: PropTypes.func.isRequired,
};

const mapStateToProps = (state, props) => ({
    game: currentGameSelector(state, props),
});

const mapDispatchToProps = (dispatch, ownProps) => ({
    onProgress: (playerId, pins) => dispatch(progress(ownProps.id, playerId, pins)),
});

export const Game = compose(
    withProps(props => ({
        id: parseInt(props.match.params.id, 10),
    })),
    connect(mapStateToProps, mapDispatchToProps),
)(DumbGame);
