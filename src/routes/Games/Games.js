import React from 'react';
import { Route } from '../../components/Route';
import { Games as GamesContainer } from '../../containers/Games';

export const Games = () => (
    <Route>
        <h1>
            Got time to <em>spare</em> for a bowl?
        </h1>
        <GamesContainer />
    </Route>
);
