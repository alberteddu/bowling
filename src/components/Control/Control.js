import React from 'react';
import PropTypes from 'prop-types';
import styles from './Control.module.css';

export const Control = ({
    children,
}) => (
    <div className={styles.control}>
        {children}
    </div>
);

Control.propTypes = {
    children: PropTypes.node.isRequired,
};
