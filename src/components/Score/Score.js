import React from 'react';
import PropTypes from 'prop-types';
import { toClass } from 'recompose';
import classnames from 'classnames';
import { Player } from '../../models/Player';
import { PlayerIcon } from '../PlayerIcon';
import { Frame } from '../Frame';
import { getPoints } from '../../functions/getPoints';
import { getFrameProps } from '../../functions/getFrameProps';
import styles from './Score.module.css';
import { getPlaceSentence } from '../../functions/getPlaceSentence';

const StatelessScore = ({
    player,
    turn,
    place,
}) => (
    <div
        className={classnames({
            [styles.score]: true,
            [styles.scoreTurn]: turn,
        })}
    >
        <div className={styles.scoreContent}>
            <div className={styles.scorePlayer}>
                <div className={styles.scorePlayerName}>
                    {player.name}
                    {place ? (
                        <div
                            className={classnames({
                                [styles.scorePlayerPlace]: true,
                                [styles.placeGold]: place === 1,
                                [styles.placeSilver]: place === 2,
                                [styles.placeBronze]: place === 3,
                            })}
                        >
                            <div className={styles.scorePlayerPlaceBadge}>{place}</div>
                            <div className={styles.scorePlayerPlaceSentence}>
                                {getPlaceSentence(place)}
                            </div>
                        </div>
                    ) : null}
                </div>
                <PlayerIcon name={player.name} size="normal" />
            </div>
            <div className={styles.scoreFrames}>
                {[...Array(10)].map((_, index) => (
                    <Frame
                        key={index}
                        {...getFrameProps(index, player.score)}
                        turn={turn}
                    />
                ))}

                <Frame
                    total
                    points={getPoints(9, player.score)}
                />
            </div>
        </div>
    </div>
);

export const Score = toClass(StatelessScore);

StatelessScore.propTypes = {
    player: Player.isRequired,
    turn: PropTypes.bool.isRequired,
    place: PropTypes.number,
};

StatelessScore.defaultProps = {
    place: null,
};
