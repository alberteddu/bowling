import React from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';
import styles from './PlayerIcon.module.css';

export const PlayerIcon = ({
    name,
    size,
}) => (
    <img
        alt=""
        className={classnames({
            [styles.icon]: true,
            [styles[size]]: true,
        })}
        src={`https://api.adorable.io/avatars/50/${name}`}
    />
);

PlayerIcon.propTypes = {
    name: PropTypes.string.isRequired,
    size: PropTypes.oneOf(['small', 'normal']),
};

PlayerIcon.defaultProps = {
    size: 'normal',
};
