import React from 'react';
import PropTypes from 'prop-types';
import { compose, withState, withProps } from 'recompose';
import generateName from 'sillyname';
import { Label } from '../Label';
import { Input } from '../Input';
import { Actions } from '../Actions';
import { Button } from '../Button';
import { Control } from '../Control';
import { Form } from '../Form';
import { PlayerIcon } from '../PlayerIcon';
import { isGameValid } from '../../functions/isGameValid';
import styles from './GameEditView.module.css';
import { Player } from '../../models/Player';

const StatelessGameEditView = ({
    title,
    setTitle,
    players,
    addPlayer,
    setPlayerName,
    removePlayer,
    onSave,
    valid,
}) => (
    <Form>
        <Control>
            <Label>Title</Label>
            <Input
                name="game-title"
                type="text"
                value={title}
                onChange={e => setTitle(e.target.value)}
            />
        </Control>

        <Control>
            <Label>Players</Label>

            {players.map(player => (
                <div className={styles.playerControl} key={player.id}>
                    <PlayerIcon size="small" name={player.name} />
                    <Input
                        type="text"
                        value={player.name}
                        onChange={e => setPlayerName(player.id, e.target.value)}
                    />
                    <button
                        className={styles.removePlayer}
                        onClick={() => removePlayer(player.id)}
                    >
                        &times;
                    </button>
                </div>
            ))}

            <Button onClick={() => addPlayer(generateName())}>Add player</Button>
        </Control>

        <Actions>
            <Button disabled={!valid} onClick={() => onSave(title, players)}>Save</Button>
        </Actions>
    </Form>
);

StatelessGameEditView.propTypes = {
    title: PropTypes.string.isRequired,
    setTitle: PropTypes.func.isRequired,
    players: PropTypes.arrayOf(Player).isRequired,
    addPlayer: PropTypes.func.isRequired,
    setPlayerName: PropTypes.func.isRequired,
    removePlayer: PropTypes.func.isRequired,
    onSave: PropTypes.func.isRequired,
    valid: PropTypes.bool.isRequired,
};

export const GameEditView = compose(
    withState('title', 'setTitle', props => props.title),
    withState('players', 'setPlayers', props => props.players),
    withProps(props => ({
        valid: (isGameValid(props.title, props.players)),
        addPlayer: (name) => {
            const ids = props.players.map(p => parseInt(p.id, 10));
            const nextId = ids.length > 0 ? Math.max(...ids) + 1 : 1;

            props.setPlayers([
                ...props.players,
                {
                    id: nextId,
                    name,
                    score: [],
                },
            ]);
        },
        removePlayer: (id) => {
            props.setPlayers(props.players.filter(p => p.id !== id));
        },
        setPlayerName: (id, name) => {
            props.setPlayers(props.players.map((p) => {
                if (p.id !== id) {
                    return p;
                }

                return {
                    ...p,
                    name,
                };
            }));
        },
    })),
)(StatelessGameEditView);
