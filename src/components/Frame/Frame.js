import React from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';
import styles from './Frame.module.css';

export const Frame = ({
    pins,
    secondChance,
    thirdChance,
    strike,
    spare,
    secondChanceStrike,
    secondChanceSpare,
    thirdChanceStrike,
    points,
    turn,
    current,
    last,
    total,
}) => (
    <div
        className={classnames({
            [styles.frame]: true,
            [styles.frameTurn]: turn,
            [styles.frameCurrent]: current,
            [styles.total]: total,
            [styles.frameLast]: last,
        })}
    >
        <div className={styles.firstRow}>
            <div className={styles.pins}>
                {!strike ? pins : null}

                {last && strike ?
                    <div className={classnames(styles.mark, styles.markStrike)} /> : null}
            </div>
            <div
                className={classnames({
                    [styles.result]: true,
                    [styles.resultStrike]: (!last && strike) || (last && secondChanceStrike),
                    [styles.resultSpare]: spare,
                })}
            >
                {!strike && !spare ? secondChance : null}
                {last && strike && !secondChanceStrike ? secondChance : null}
                {strike || spare ? <div className={styles.mark} /> : null}
            </div>
            {last ? (
                <div
                    className={classnames({
                        [styles.extra]: true,
                        [styles.extraStrike]: secondChanceStrike,
                        [styles.extraSpare]: secondChanceSpare,
                    })}
                >
                    {!thirdChanceStrike && !secondChanceSpare ? thirdChance : null}
                    {thirdChanceStrike || secondChanceSpare ?
                        <div className={styles.mark} /> : null}
                </div>
            ) : null}
        </div>
        <div className={styles.secondRow}>
            <div className={styles.points}>
                {points}
            </div>
        </div>
    </div>
);

Frame.propTypes = {
    pins: PropTypes.number,
    secondChance: PropTypes.number,
    thirdChance: PropTypes.number,
    strike: PropTypes.bool,
    spare: PropTypes.bool,
    secondChanceStrike: PropTypes.bool,
    secondChanceSpare: PropTypes.bool,
    thirdChanceStrike: PropTypes.bool,
    points: PropTypes.number,
    turn: PropTypes.bool,
    current: PropTypes.bool,
    last: PropTypes.bool,
    total: PropTypes.bool,
};

Frame.defaultProps = {
    pins: null,
    secondChance: null,
    thirdChance: null,
    strike: false,
    spare: false,
    secondChanceStrike: false,
    secondChanceSpare: false,
    thirdChanceStrike: false,
    points: null,
    turn: false,
    current: false,
    last: false,
    total: false,
};
