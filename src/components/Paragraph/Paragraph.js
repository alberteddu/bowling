import React from 'react';
import PropTypes from 'prop-types';
import styles from './Paragraph.module.css';

export const Paragraph = ({
    children,
}) => (
    <p className={styles.p}>{children}</p>
);

Paragraph.propTypes = {
    children: PropTypes.node.isRequired,
};
