import React from 'react';
import PropTypes from 'prop-types';
import styles from './Label.module.css';

export const Label = ({
    children,
    ...rest
}) => (
    <label className={styles.label} {...rest}> {/* eslint-disable-line jsx-a11y/label-has-for */}
        {children}
    </label>
);

Label.propTypes = {
    children: PropTypes.node.isRequired,
};
