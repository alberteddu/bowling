import React from 'react';
import PropTypes from 'prop-types';
import styles from './Button.module.css';

export const Button = ({
    children,
    ...rest
}) => (
    <button className={styles.button} {...rest}>{children}</button>
);

Button.propTypes = {
    children: PropTypes.node.isRequired,
};
