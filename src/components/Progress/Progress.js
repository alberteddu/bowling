import React from 'react';
import PropTypes from 'prop-types';
import { withState } from 'recompose';
import Slider from 'rc-slider';
import 'rc-slider/assets/index.css';
import { PlayerProps } from '../../models/Player';
import { FIRST_ATTEMPT, SECOND_ATTEMPT, THIRD_ATTEMPT } from '../../functions/getBallAttempt';
import { getAttemptString } from '../../functions/getAttemptString';
import { Button } from '../Button';
import styles from './Progress.module.css';

const DumbProgress = ({
    player,
    max,
    ball,
    onProgress,
    pins,
    setPins,
}) => (
    <div className={styles.progress}>
        <p>
            It&apos;s <strong>{player.name}</strong>&apos;s turn.
            How many pins for your {getAttemptString(ball)}?
        </p>

        <div className={styles.slider}>
            <Slider
                value={pins}
                onChange={setPins}
                dots
                max={max}
                marks={[...Array(max + 1)].reduce((accumulator, _, index) => ({
                    ...accumulator,
                    [index]: index,
                }), {})}
                trackStyle={{ backgroundColor: 'rgba(0, 0, 0, .4)' }}
                railStyle={{ backgroundColor: '#E2E2D8' }}
                dotStyle={{ backgroundColor: '#DADAD0', border: 0 }}
                activeDotStyle={{ backgroundColor: '#888882', border: 0 }}
                handleStyle={{ backgroundColor: '#888882', border: 0 }}
            />
        </div>

        <div className={styles.actions}>
            <Button
                onClick={() => {
                    onProgress(player.id, pins);
                    setPins(0);
                }}
            >
                Progress
            </Button>
            <Button
                onClick={() => {
                    onProgress(player.id, Math.floor(Math.random() * max) + 1);
                    setPins(0);
                }}
            >
                Randomize
            </Button>
        </div>
    </div>
);

export const Progress = withState('pins', 'setPins', 0)(DumbProgress);

Progress.propTypes = {
    player: PropTypes.shape(PlayerProps).isRequired,
    max: PropTypes.number,
    ball: PropTypes.oneOf([FIRST_ATTEMPT, SECOND_ATTEMPT, THIRD_ATTEMPT]).isRequired,
    onProgress: PropTypes.func.isRequired,
};

Progress.defaultProps = {
    max: 10,
};

DumbProgress.propTypes = {
    ...Progress.propTypes,
    pins: PropTypes.number.isRequired,
    setPins: PropTypes.func.isRequired,
};
