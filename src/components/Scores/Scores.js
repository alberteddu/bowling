import React from 'react';
import PropTypes from 'prop-types';
import { withProps } from 'recompose';
import FlipMove from 'react-flip-move';
import { Player, PlayerProps } from '../../models/Player';
import { Score } from '../Score';
import { findTurnPlayer } from '../../functions/findTurnPlayer';
import { IN_PROGRESS, OVER } from '../../models/Game';
import { Progress } from '../Progress';
import {
    getBallAttempt, NO_MORE_ATTEMPTS,
    SECOND_ATTEMPT,
    THIRD_ATTEMPT,
} from '../../functions/getBallAttempt';
import styles from './Scores.module.css';
import { isGameOver } from '../../functions/isGameOver';
import { getPoints } from '../../functions/getPoints';

const DumbScores = ({
    players,
    player,
    state,
    ball,
    max,
    onProgress,
}) => (
    <div className={styles.scores}>
        {state === IN_PROGRESS ? (
            <Progress
                player={player}
                ball={ball}
                onProgress={onProgress}
                max={max}
            />
        ) : null}

        <FlipMove duration={250}>
            {players.map(p => (
                <Score
                    key={p.id}
                    player={p}
                    turn={state === IN_PROGRESS && p.id === player.id}
                    place={p.place}
                />
            ))}
        </FlipMove>
    </div>
);

DumbScores.propTypes = {
    players: PropTypes.arrayOf(Player).isRequired,
    player: PropTypes.shape(PlayerProps),
    state: PropTypes.oneOf([IN_PROGRESS, OVER]).isRequired,
    ball: PropTypes.string,
    onProgress: PropTypes.func.isRequired,
    max: PropTypes.number,
};

DumbScores.defaultProps = {
    player: null,
    ball: NO_MORE_ATTEMPTS,
    max: 10,
};

export const Scores = withProps(({ players }) => {
    const gameOver = isGameOver(players);

    if (gameOver) {
        players.sort((a, b) => getPoints(9, b.score) - getPoints(9, a.score));

        return {
            players: players.map((p, index) => ({
                ...p,
                place: index + 1,
            })),
        };
    }

    const index = findTurnPlayer(players);
    let newPlayers = [...players];
    let currentIndex = 0;
    let max = 10;

    while (index !== currentIndex) {
        newPlayers = [...newPlayers.slice(1), newPlayers[0]];

        currentIndex += 1;
    }

    const player = newPlayers[0];
    const { score } = player;

    let current = score[score.length - 1];

    if (!current || (score.length < 10 && current.length === 2)) {
        current = [];
    }

    const last = score.length === 10 || (score.length === 9 && score[8].length === 2);
    const ball = getBallAttempt(current, last);

    if (ball === SECOND_ATTEMPT && (!last || current[0] < 10)) {
        max = 10 - current[0];
    }

    if (ball === THIRD_ATTEMPT && current[1] < 10) {
        max = 10 - current[1];
    }

    return {
        players: newPlayers,
        player,
        ball,
        max,
    };
})(DumbScores);
