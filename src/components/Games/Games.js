import React from 'react';
import PropTypes from 'prop-types';
import { Game } from '../Game';
import { Game as GameModel } from '../../models/Game';
import { NewGameCard } from '../../containers/NewGameCard';
import styles from './Games.module.css';

export const Games = ({
    games,
}) => (
    <div className={styles.games}>
        <NewGameCard />
        {games.map(game => (
            <Game key={game.id} {...game} />
        ))}
    </div>
);

Games.propTypes = {
    games: PropTypes.arrayOf(GameModel).isRequired,
};
