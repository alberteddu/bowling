import React from 'react';
import PropTypes from 'prop-types';
import styles from './CardAction.module.css';

export const CardAction = ({
    label,
    action,
}) => (
    <div className={styles.cardAction}>
        <button className={styles.button} onClick={action}>
            <span className={styles.label}>{label}</span>
        </button>
    </div>
);

CardAction.propTypes = {
    label: PropTypes.string.isRequired,
    action: PropTypes.func.isRequired,
};
