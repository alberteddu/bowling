import React from 'react';
import classnames from 'classnames';
import { GameProps, IN_PROGRESS, OVER } from '../../models/Game';
import styles from './Game.module.css';
import { PlayerIcon } from '../PlayerIcon';

export const Game = ({
    id,
    name,
    players,
    state,
}) => (
    <div className={styles.game}>
        <a
            className={classnames({
                [styles.link]: true,
                [styles.linkOver]: state === OVER,
            })}
            href={`/#/g/${id}`}
        >
            <span className={styles.title}>{name}</span>

            <div className={styles.state}>
                {state === IN_PROGRESS ? 'Game in progress' : 'Game over'}
            </div>

            <div className={styles.players}>
                {players.map(player => (
                    <PlayerIcon key={player.id} size="small" name={player.name} />
                ))}
            </div>
        </a>
    </div>
);

Game.propTypes = GameProps;
