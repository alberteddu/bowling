import React from 'react';
import PropTypes from 'prop-types';

export const Form = ({
    children,
}) => (
    <div>
        {children}
    </div>
);

Form.propTypes = {
    children: PropTypes.node.isRequired,
};
