import React from 'react';
import PropTypes from 'prop-types';
import styles from './Route.module.css';

export const Route = ({ children }) => (
    <div className={styles.route}>
        {children}
    </div>
);

Route.propTypes = {
    children: PropTypes.node.isRequired,
};
