import PropTypes from 'prop-types';
import { Player } from './Player';

export const IN_PROGRESS = 'GAME_STATE/IN_PROGRESS';
export const OVER = 'GAME_STATE/OVER';

export const GameProps = {
    id: PropTypes.number.isRequired,
    name: PropTypes.string.isRequired,
    players: PropTypes.arrayOf(Player),
    state: PropTypes.oneOf([IN_PROGRESS, OVER]).isRequired,
};
export const Game = PropTypes.shape(GameProps);

