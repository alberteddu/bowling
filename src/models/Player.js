import PropTypes from 'prop-types';

export const PlayerProps = {
    id: PropTypes.number.isRequired,
    name: PropTypes.string.isRequired,
};

export const Player = PropTypes.shape(PlayerProps);
