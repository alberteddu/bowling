import React from 'react';
import { render } from 'react-dom';
import { Provider } from 'react-redux';
import { store } from './store';
import { App } from './containers/App';
import './styles/app.css';

const target = document.getElementById('app');
const mount = Component => render(
    <Provider store={store}>
        <Component />
    </Provider>,
    target,
);

if (process.env.NODE_ENV === 'development') {
    module.hot.accept('./containers/App', () =>
        mount(require('./containers/App').App)); // eslint-disable-line global-require
}

mount(App);
